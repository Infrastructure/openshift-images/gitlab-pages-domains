#!/usr/bin/env python3

import os
from string import Template
from urllib.parse import urlparse

try:
    import tomllib
except ModuleNotFoundError:
    import tomli as tomllib

if __name__ == "__main__":
    with open("nginx.conf.tpl") as fp:
        template = Template(fp.read())

    files = os.scandir("pages")
    for file in files:
        with open(file, "rb") as fp:
            config = tomllib.load(fp)

        for page in config["pages"]:
            url = urlparse(page["origin"])
            domain = page["domain"].split(" ")[0]
            subst = {
                "domains": page["domain"],
                "domain": domain,
                "filename": file.name,
                "host": url.netloc,
                "path": url.path,
                "custom": page.get("custom", ""),
            }

            nginx_config = template.substitute(subst)
            with open(f"conf.d/{domain}.conf", "w") as fp:
                fp.write(nginx_config)
