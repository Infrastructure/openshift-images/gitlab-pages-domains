# Generated from $filename
server {
    listen 8080;
    server_name $domains;

    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-Host "";
        proxy_redirect //$host$path https://$domain/;
        proxy_pass https://production-gitlab-pages.pages.gitlab.gnome.org$path;

        proxy_ssl_server_name on;
        proxy_ssl_name production-gitlab-pages.pages.gitlab.gnome.org;
    }
$custom
}
