FROM docker.io/library/python:3.11-slim
ADD . /app
WORKDIR /app
RUN python generate.py

FROM docker.io/nginxinc/nginx-unprivileged
RUN rm -rf /etc/nginx/conf.d
COPY --from=0 /app/conf.d /etc/nginx/conf.d
